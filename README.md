# 龙行天下数据导出

## 需求:

数据组朱林需求

## 数据源

线上阿里云redis服务器

## 约束

redis存储zset数据被DS采用huffman编码压缩，非通用

## 数据源

李萌提供

内网测试环境:

redis: 192.168.3.146:6379
db:    15
key:   DragonGoods_yyyymmdd

线上阿里云:

redis:
139.224.33.172:6379
139.196.190.26:6379

db:    15
key:   DragonGoods_yyyymmdd

## 未解决问题

李萌处数据有分组信息缺失问题。
问题描述详见: doc/data_schema_and_problems.html
