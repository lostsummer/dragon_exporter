#ifndef __SYSGLOBAL_H__
#define __SYSGLOBAL_H__

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <assert.h>
#include <pthread.h>
#include <string.h>

#define PAGE_SIZE		8192

// data types
typedef char CHAR;
typedef char* PCHAR;
typedef unsigned char BYTE;
typedef unsigned char UCHAR;
typedef unsigned char* PBYTE;
typedef unsigned short WORD;
typedef unsigned short* PWORD;
typedef unsigned int DWORD;
typedef unsigned int* PDWORD;
typedef int INT;
typedef int INT32;
typedef unsigned int UINT;
typedef unsigned int UINT32;
typedef long long INT64;
typedef unsigned long long UINT64;
typedef long LONG;
typedef bool BOOL;

typedef char TCHAR;
typedef char* LPSTR;
typedef char* LPTSTR;
typedef const char* LPCWSTR;
typedef const char* LPCTSTR;

// consts
#define TRUE	true
#define FALSE	false

#define Max(a,b) (((a)>(b))?(a):(b)) 
#define Min(a,b) (((a)<(b))?(a):(b)) 

#define ASSERT	assert
#define CopyMemory memcpy
#define MoveMemory memmove
#define ZeroMemory(a, b) memset(a, 0, b)

#ifndef ARRAY_LENGTH
#define ARRAY_LENGTH(arr) (sizeof(arr) / sizeof(*arr))
#endif

#define MINUTE 60
#define HOUR (60*MINUTE)
#define DAY (24*HOUR)
#define YEAR (365*DAY)
#define TM_ZONE   8          //中国适用，东八区
static int month[12] = {
	0,
	DAY*(31),
	DAY*(31+29),
	DAY*(31+29+31),
	DAY*(31+29+31+30),
	DAY*(31+29+31+30+31),
	DAY*(31+29+31+30+31+30),
	DAY*(31+29+31+30+31+30+31),
	DAY*(31+29+31+30+31+30+31+31),
	DAY*(31+29+31+30+31+30+31+31+30),
	DAY*(31+29+31+30+31+30+31+31+30+31),
	DAY*(31+29+31+30+31+30+31+31+30+31+30)
};
#endif


