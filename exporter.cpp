#include <stdlib.h>
#include <string.h>
#include "lua_tinker.h"
#include "Compress.h"
#include "XInt32.h"

#pragma pack(push, 1)
struct CDragonRecordEx
{
    CDragonRecordEx()
    {
        ZeroMemory(this, sizeof(CDragonRecordEx));
    }
    DWORD m_dwDate;         //YYMMDD
    WORD m_wTime;           //hhmm
    DWORD m_dwGoodsID;      // GoodsID
    union
    {
        //板块
        struct
        {           
            int m_iInputCount;      //个股价格
            int m_nAveZDF;          //板块里满足条件个股的平均涨跌幅
            
        };
        //个股
        struct
        {
            DWORD m_dwGroupID;      // GroupID，板块ID，对个股，表示在哪个板块里
            DWORD m_dwPrice;        // 个股价格
            int m_nZDF;             // 2位小数，个股涨跌幅，板块里满足条件个股的平均涨跌幅
            DWORD m_xAmount;        // 个股成交额，板块无意义
        };
    };
    int m_nSumZDF;          // 2位小数, 个股每分钟涨跌幅的和
    DWORD m_dwPos;          // 涨跌的序号

    
};
#pragma pack(pop)

CCompress comp;
int main(int argc, char *argv[])
{
    char date[9] = {0};
    if (argc > 1)
    {
        strncpy(date, argv[1], 8);
    }

    lua_State * pLuaState = lua_open();   
    luaL_openlibs(pLuaState);
    lua_tinker::dofile(pLuaState, "worker.lua");
    lua_tinker::set(pLuaState, "date", date);
    lua_tinker::call<void>(pLuaState, "getredisdata");
    lua_tinker::table res = lua_tinker::get<lua_tinker::table>(pLuaState, "res");
    int reslen = lua_tinker::get<int>(pLuaState, "reslen");

    lua_tinker::table lgoods = lua_tinker::get<lua_tinker::table>(pLuaState, "lgoods");

    for (int i=1; i<=reslen; i++)
    {
        static int idx = 0;
        lua_tinker::table rec = res.get<lua_tinker::table>(i);

        int time = rec.get<int>(2);
        const char *pLuabuf = rec.get<const char*>(1);
        int inLen = *(int *)pLuabuf;
        if (inLen <= 0 || inLen > 10240)
        {
            continue;
        }
        comp.m_lIn = inLen;
        comp.m_pcIn = (char*)pLuabuf + 4;
        comp.Decode();
        int outLen = comp.m_lOut;

        char * outBuf = comp.m_pcOut;
        int goodsNum = *(int*)(outBuf+2);
        if (goodsNum * sizeof(CDragonRecordEx) > outLen - 10)
        {
            printf("outLen: %d  goodsNum: %d size: %d\n", outLen, goodsNum, sizeof(CDragonRecordEx));
            continue;
        }

        CDragonRecordEx *pDragon = (CDragonRecordEx *)(outBuf + 10);

        for (int j=0; j<goodsNum; j++)
        {
            lua_tinker::table goods(pLuaState);
            goods.set("time", time);
            goods.set("goodsid", pDragon[j].m_dwGoodsID);
            goods.set("zdf", pDragon[j].m_nZDF);
            goods.set("groupid", pDragon[i].m_dwGroupID);
            lgoods.set(++idx, goods);
        }
    }

    lua_tinker::call<void>(pLuaState, "output");
    return 0;
}




