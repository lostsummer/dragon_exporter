#!/usr/bin/lua

redis = require 'redis'
json = require 'json'
config = require 'config'

date=""
res={}
reslen=0
lgoods = {}
goods = {}
local f = assert(io.open("codemap.json", "r"))
local jstr = f:read()
f:close()
local codemap = json.decode(jstr)

function getredisdata()
    local cli = redis.connect(config.redis_dragon.host, config.redis_dragon.port)
    cli:select(15)
    if date == nil or #date < 8 then
        date = os.date("%Y%m%d")
    end
    key = string.format("DragonGoods_%s", date)
    res = cli:zrangebyscore(key, 
                            931, 
                            1500, 
                            {withscores=true})
    reslen = #res
end


function output()
    local y = string.sub(date, 1, 4)
    local m = string.sub(date, 5, 6)
    local d = string.sub(date, 7, 8)
    local date_i = string.format("%s-%s-%s",y, m, d)
    local fname = string.format("Dragon_%s.csv", date)
    local fout = io.open(fname, "w")
    headline = "序号, 日期, 时间, 板块代码, 板块名称, 股票代码, 股票名称, 涨跌幅\n"
    fout:write(headline)
    for i, v in ipairs(lgoods) do
        no = i
        time = string.format("%02d:%02d", v.time/100, v.time%100) 
        if v.groupid > 0 then
            groupid = string.format("%07d", v.groupid)
            groupcode = codemap[groupid]["code"]
            groupname = codemap[groupid]["name"]
        else
            groupcode = "0"
            groupname = " "
        end
        goodsid = string.format("%07d", v.goodsid)
        goodscode = codemap[goodsid]["code"]
        goodsname = codemap[goodsid]["name"]
        zdf = v.zdf/100.0
        line = string.format("%d, %s, %s, %s, %s, %s, %s, %.02f\n", no, date_i, time, groupcode, groupname, goodscode, goodsname, zdf)
        fout:write(line)
    end
    fout:close()
end
