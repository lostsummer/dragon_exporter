require "logging"
local config = {
    redis_code = {
        host = "192.168.42.84",
        port = 6379
    },

    redis_dragon = {
        host = "139.224.33.172",
        port = 6379
    },

    --[[redis_dragon = {
        host = "139.196.190.26",
        port = 6379
    },]]--

    log = {
        level = logging.INFO,
        dir = 'log'
    }
}

return config
